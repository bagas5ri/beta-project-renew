<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Team;
use Faker\Generator as Faker;

$factory->define(Team::class, function (Faker $faker) {
    return [
        'user_id' => 1,
        'role' => $faker->randomElement(['anggota', 'penanggung_jawab', 'ketua'])
    ];
});
