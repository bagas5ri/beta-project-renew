<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

use Faker\Factory as Faker;

class TeamSeeder extends Seeder
{
    
    // public function run()
    // {
    //     // factory(\App\Team::class, 20)->create();

    //     $faker = Faker::create('id_ID');
        
    //     $user = DB::table("users")->select("id")->get()->toArray();

    //     //extract id to $member
    //     $member = [];
    //     foreach ($user as $key => $value) {
    //         $member[$key] = $value->id;
    //     }
    //     for ($i=0; $i < 50; $i++) { 
    //         $usr = $faker->randomElement($member);
    //         $max = count(DB::table("teams")->where("user_id", $usr)->get()->toArray())>3 ? true : false;
    //         if(!$max){
    //             DB::table("teams")->insert([
    //                 'user_id' => $usr,
    //                 'role' => $faker->randomElement(['anggota', 'penanggung_jawab', 'ketua'])
    //             ]);
    //         }
    //     }
    // }

    public function run(){
        $user = App\User::select("id")->orderBy("id")->get()->toArray();
        //extract id to $member
        $member = [];
        foreach ($user as $key => $value) {
            $member[$key+5] = $value["id"];
        }
        
        $total = count($member)+5;
        //iterasi extract 5 10 15 20 25 
         for ($i=5; $i < $total; $i++) {
            $i_team= $i/5; //Extract iteration -> 1 2 3 4 5
            $penanggung_jawab = $member[$i]; //member[4] = 1
            $ketua = $member[$i+1]; //member[5] = 2

            //Create penanggung jawab & ketua
            App\Team::create([
                'ketua' => $ketua,
                'penanggung_jawab' => $penanggung_jawab
            ]);

            //Create anggota
            for ($x=$i-4; $x < $i+1; $x++) {
                //x = 1 2 3 4
                $role = "";
                switch ($x) {
                    case $i-4:
                        $role = "penanggung_jawab";
                        break;
                    case $i-3:
                        $role = "ketua";
                        break;
                    default:
                        $role = "anggota";
                        break;
                }
                //penanggungJawab
                App\Employee::create([
                        'team_id' => $i_team,
                        'user_id' => $member[$x+4],
                        'role' => $role
                    ]);
            }
            $i+=4;
        }
        
    }
}
