<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        //++++      Only Config on UsersTableSeeder.php     ++++
        //++++      Only Config on UsersTableSeeder.php     ++++
        //++++      Only Config on UsersTableSeeder.php     ++++
        //++++      Only Config on UsersTableSeeder.php     ++++
        //              And run
        //      php artisan migrate:refresh --seed
        //      need migrate:refresh because seeder cant run multiple time, yet !!

        //Create user hinata
        App\User::create([
            'name' => 'Hinata Putih',
            'email' => 'hinata@naruto.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => rand(1000000000, 9999999999)
        ]);

        //Register Seeder
        $this->call(UsersTableSeeder::class);
        $this->call(TeamSeeder::class); //include employee


        //Test more then 2 user at team 1
        App\Employee::create([
            'team_id' => 1,
            'user_id' => 5,
            'role' => 'anggota'
        ]);
        App\Employee::create([
            'team_id' => 1,
            'user_id' => 6,
            'role' => 'anggota'
        ]);
        App\Employee::create([
            'team_id' => 1,
            'user_id' => 7,
            'role' => 'anggota'
        ]);
    }
}
