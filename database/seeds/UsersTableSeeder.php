<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        //Configure $total only
        $total = 5; //min 3

        $u_total = ($total*5)-1;
        factory(\App\User::class, $u_total)->create();
    }
}
