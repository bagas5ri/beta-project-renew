@extends('layouts.app')
@section('content')
<div class="container">
<div class="row justify-content-center">
<div class="col-md-10">
<div class="card">
<div class="card-header">Table</div>

<div class="card-body">    
    <div class="table-responsive">
        <table class="table">
            <caption>List of Team</caption>
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Ketua</th>
                <th scope="col">Penanggung Jawab</th>
                <th scope="col">Anggota</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody>
                @foreach ($data as $index_team => $team)  
                <tr>
                    <th scope="row">{{$index_team+1}}</th>
                    <td>{{$team->ketua()->first()->name}}</td>
                    <td>{{$team->penanggung_jawab()->first()->name}}</td>
                    {{-- <td>{{$team->employee()->get()}}</td> --}}
                    {{-- <td>{{$team['id']}}</td> --}}
                    {{-- <td>{{$employee->where('team_id', 4)}}</td> --}}
                    {{-- <td>{{Auth::user()->id}}</td> --}}
                    <td>
                        @foreach ($employee->where('team_id', $team['id']) as $anggota)
                            {{$anggota->user()->first()->name}}<hr>
                        @endforeach
                    </td>
                    {{-- <td>{{$team->ketua}}</td> --}}

                    @if (Auth::user()->email == $team->penanggung_jawab()->first()->email)
                    <td>
                        <a href="editteam/{{$team->id}}" class="btn btn-info btn-lg" role="button" aria-pressed="true">Edit</a>
                        <a href="destroy/{{$team->id}}" class="btn btn-dark btn-lg" role="button" aria-pressed="true">Remove</a>

                    </td>
                    @else
                    <td>-</td>
                    @endif
                </tr>
                @endforeach
            </tbody>
        </table>
      </div>
</div>

</div>
</div>
</div>
</div>
@endsection