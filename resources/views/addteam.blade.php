@extends('layouts.app')
@section('content')
<div class="container">
<div class="row justify-content-center">
<div class="col-md-10">
<div class="card">
<div class="card-header">Add Team</div>

<div class="card-body">

<form method="POST" action="/addteam">
    @csrf
    <div class="form-group row">
        <label for="penanggung_jawab" class="col-md-4 col-form-label text-md-right">Penanggung Jawab</label>
        <div class="col-md-6">

            <select id="penanggung_jawab" class="form-control" name="penanggung jawab" readonly>
                <option value="{{Auth::user()->id}}">{{Auth::user()->name}}</option>
            </select>
        </div>
      </div>

    <div class="form-group row">
        <label for="inputKetua" class="col-md-4 col-form-label text-md-right">Ketua</label>
        <div class="col-md-6">
            <select name="ketua" class="form-control" id="inputKetua">
                @foreach ($employee as $anggota)
                    <option value="{{$anggota->id}}">{{$anggota->name}}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group row">
        <label for="inputAnggota" class="col-md-4 col-form-label text-md-right">Anggota</label>
        <div class="col-md-6">
            <select name="anggota[]" class="form-control  mb-1" id="inputAnggota">
                @foreach ($employee as $anggota)
                    <option value="{{$anggota->id}}">{{$anggota->name}}</option>
                @endforeach
            </select>
            <select name="anggota[]" class="form-control  mb-1" id="inputAnggota">
                @foreach ($employee as $anggota)
                    <option value="{{$anggota->id}}">{{$anggota->name}}</option>
                @endforeach
            </select>
            <select name="anggota[]" class="form-control  mb-1" id="inputAnggota">
                @foreach ($employee as $anggota)
                    <option value="{{$anggota->id}}">{{$anggota->name}}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group row">
        <div class="col offset-md-2 text-center">
            <button type="button" class="btn btn-primary" id="addField">
                Add More Field
            </button>
        </div>
    </div>
    
    <div class="form-group row mb-0">
        <div class="col-md-8 offset-md-4">
            <button type="submit" class="btn btn-primary">
                Submit
            </button>
        </div>
    </div>
</form>

</div>
</div>
</div>
</div>
</div>
@endsection

<script>

    document.addEventListener('DOMContentLoaded', function(){
        btn_add_field = document.querySelector("#addField")
        btn_add_field.addEventListener('click', function(){
            AddButton();
        })
    })
    
    
    function AddButton(){
        place = document.querySelector("[name='anggota[]']").parentElement;
        console.log("jejeje");
        document.querySelector("body")
        data = {!! json_encode($employee) !!}

        el_select = document.createElement("select");
        el_select.setAttribute("name","anggota[]")
        el_select.setAttribute("class", "form-control  mb-1")
        el_select.setAttribute("id", "inputAnggota")
        
        data.forEach(option_data => {
            el_option = document.createElement("option");
            el_option.setAttribute("value", option_data.id)
            option_text = document.createTextNode(option_data.name)
            el_option.appendChild(option_text)
            el_select.appendChild(el_option)
        });

        place.appendChild(el_select)

        console.log(el_select)
                
    }

    
</script>