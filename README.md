## About Aplikasi Manajemen Tim / Kelompok Kerja

Sebuah perusahaan ingin membuat sistem yang dimaksudkan untuk mengelola tim atau kelompok kerja. Pengelolaan tim tersebut dilakukan agar perusahaan mengetahui detail struktur dari masing-masing tim mulai dari penanggung jawab, ketua dan anggota.

## Task
Tugas teman2 adalah membuat aplikasi manajemen tim / kelompok kerja dengan ketentuan aplikasi sebagai berikut
- [X]	Dalam sebuah tim terdapat satu penanggung jawab
- [X]	Dalam sebuah tim terdapat satu ketua
- [X]	Dalam sebuah tim terdapat minimal 3 anggota
- [X]	Satu pegawai hanya dapat mengisi satu role dalam sebuah tim.
- [X]	Satu pegawai hanya dapat terdaftar pada maksimal 2 tim saja.
- [X]	Semua pegawai dapat mendaftarkan timnya masing-masing.
- [X]	Pegawai yang mendaftarkan timnya secara otomatis akan menjadi penanggung jawab dari tim tersebut.
- [X]	Setiap Pegawai dapat melihat semua tim yang telah terdaftar.
- [X]	Hanya penanggung jawab yang dapat merubah dan menghapus timnya.

Ketentuan teknis pembuatan aplikasi
- [X]	Project dibuat menggunakan framework Laravel
- [X]	Perhatikan validasi data.
- [X]	User Interface sesuai preferensi masing2.
- [X]	Silahkan gunakan laravel authentication untuk membuat sistem autentikasi.
- [X]	Diperbolehkan menggunakan plugin
- [X]	Silahkan gunakan seeder/faker atau plugin lainya untuk membuat data pegawai (tidak perlu ada manajemen user/pegawai)
- [X]	Gunakan Laravel Migration untuk majemen database.
- [X]	Gunakan Laravel Eloquent untuk berinteraksi dengan database.
- [X]	Maksimalkan konsep MVC yang disediakan oleh Laravel secara baik dan benar.
- [X]	Untuk Projeknya silahkan di-push ke Gitlab.
