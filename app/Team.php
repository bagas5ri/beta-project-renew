<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $fillable = [
        "ketua",
        "penanggung_jawab"
    ];

    //memiliki banyak user
    // public function User(){
    //     return $this->hasMany(User::class, "id", "ketua");
    // }
    //dimiliki banyak employee
    public function Employee(){
        return $this->belongsTo(Employee::class, "team_id", "id");
    }
    public function ketua(){
        return $this->hasMany(User::class, "id", "ketua");
    }
    public function penanggung_jawab(){
        return $this->hasMany(User::class, "id", "penanggung_jawab");
    }

    // public function User(){
    //     return collect($this->ketua(), $this->penanggung_jawab());
    // }
}
