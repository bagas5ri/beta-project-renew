<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'name', 'email', 'password',
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    
    //dimiliki banyak team
    public function Team(){
        return $this->belongsTo(Team::class, "ketua", "id");
    }

    //dimiliki banyak employee
    public function Employee(){
        return $this->belongsTo(Employee::class);
    }
    
}
