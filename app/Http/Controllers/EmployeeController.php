<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\Team;
use App\User;
use Illuminate\Support\Facades\Auth;
use phpDocumentor\Reflection\Types\Null_;

class EmployeeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $data = [
            "data" => $this->data(),
            "employee" => Employee::whereNotIn('role', ['penanggung_jawab', 'ketua'])->with('User')->get()
        ];
        return view('table', $data);
    }

    public function addteam(){
        $data = [
            "employee" => $this->available_for_team()
        ];
        return view('addteam', $data);
    }


    public function store(Request $request){
        
        //if request not valid
        if($this->input_check($request)){
            return redirect('table');
        }
        
        // print_r($request->anggota);
        // die();

        $team = new Team;
        $team->ketua = $request->ketua;
        $team->penanggung_jawab = $request->penanggung_jawab;
        $team->save(); //id store in $team->id after save()
        
        //insert employee
        foreach ($request->all() as $key => $anggota) {
            echo "$key";
            if($key == "ketua"){
                Employee::create([
                    "team_id" => $team->id,
                    "user_id" => $anggota,
                    "role" => "ketua"
                ]);
            }elseif($key == "penanggung_jawab"){
                Employee::create([
                    "team_id" => $team->id,
                    "user_id" => $anggota,
                    "role" => "penanggung_jawab"
                ]);
            }elseif($key == "anggota"){
                foreach ($anggota as $in_anggota) {
                    echo "anggota<br>|";

                    Employee::create([
                        "team_id" => $team->id,
                        "user_id" => $in_anggota,
                        "role" => "anggota"
                    ]);
                }
            }
        }

        return redirect('table');
    }

    public function editteam(Request $req, $id){

        $user_id_result = [];
        $user_id_result["anggota"] = [];
        $user_id_result["ketua"] = Employee::select("user_id")->where('team_id', "=", $id, "AND")->where('role', 'ketua')->get()->toArray()[0];
        $user_id_anggota = Employee::select("user_id")->where('team_id', "=", $id, "AND")->where('role', 'anggota')->get()->toArray();
        // $user_id_penanggung_jawab = Employee::select("user_id")->where('team_id', $id)->get()->toArray();
        foreach ($user_id_anggota as $value) {
            array_push($user_id_result["anggota"], $value["user_id"]);
        }

        $data = [
            "data" => $user_id_result,
            "employee" => $this->available_for_team()
        ];
        // return "<pre>".$data["employee"]."</pre>";
        return view("editteam", $data);
    }

    public function update(Request $req){
        //Update logic
        if(!$this->input_check($req, true)){
            Team::where("id", $req->team_id)
                ->update([
                    'ketua' => $req->ketua,
                    'penanggung_jawab' => $req->penanggung_jawab
                ]);
            //Flush Anggota in employee
            Employee::where('team_id', "=", $req->team_id, "AND")
                ->where("role", "!=", "penanggung_jawab")
                ->delete();

            //Add Ketua
            Employee::create([
                'team_id' => $req->team_id,
                'user_id' => $req->ketua,
                'role' => 'ketua'
            ]);

            foreach ($req->anggota as $id_anggota) {
                Employee::create([
                    'team_id' => $req->team_id,
                    'user_id' => $id_anggota,
                    'role' => 'anggota'
                ]);
            }
        }
        return redirect("table");
    }

    public function destroy($id){
        Employee::where("team_id", $id)->delete();
        Team::where('id', $id)->delete();
        return redirect("table");
    }





    public function input_check($req, $par=false)
    {
        $status = [];
        //Input not include
        if(empty($req->ketua) || empty($req->penanggung_jawab)){
            return $status["primary_null"] = true;
        }

        //Block cross input with check Penanggung Jawab
        if(!$this->isPenanggungJawab($req->penanggung_jawab)){
            return $status["tryHack"] = true;
        }

        if(count($req->anggota) < 3){
            return $status['not_enough'] = true;
        };
        
        //prepare variable anggota for check
        $well_anggota  = []; //[5,2,1,3,4]
        array_push($well_anggota, $req->ketua);
        array_push($well_anggota, $req->penanggung_jawab);
        if(!empty($req->anggota)){
            foreach ($req->anggota as $value) {
                array_push($well_anggota, $value);
            }
        }else{
            $status["anggota_empty"] = true;
            return $status;
        }

        //!anggota
        !count($well_anggota) ? $status["empty"] = true : "";

        //check Duplicate
        (count($well_anggota) != count(array_unique($well_anggota))) ? $status["isDuplicat"]=true : "";

        //par true from update
        if($par){
            //Check team more then 2
            foreach ($well_anggota as $value) {
                //if anggota not in the same team
                if(!Employee::where("user_id", "=", $value, "AND")->where('team_id', $req->team_id)->count()){
                    Employee::where("user_id", "=", $value)->count() > 1 ? $status["team_max"][$value]=true: "";
                }
            }
        }else{
            foreach ($well_anggota as $value) {
                Employee::where("user_id", "=", $value)->count() > 1 ? $status["team_max"][$value]=true: "";
            }
        }

        return $status;
    }

    public function isPenanggungJawab($penanggung_jawab){
        return Auth::user()->id == $penanggung_jawab;
    }


    public function data(){
        return Team::with('ketua', 'penanggung_jawab')->get();
    }

    public function available_for_team(){
        // return Employee::select(["id","team_id"])->groupBy("team_id")->orderBy("team_id")->each(function($key){ #code .....});

        //Team more then 2
        return User::get()->map(function($key){
            return Employee::where("id", $key->id)->count() < 2 ? $key : NULL;    
        });

    }
}
