<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = [
        "team_id",
        "role",
        "user_id"
    ];

    protected $attribute = [
        "team_id" => NULL
    ];

    //memiliki banyak user
    public function User(){
        return $this->hasMany(User::class, "id", "user_id");
    }
    //memiliki banyak team
    public function Team(){
        return $this->hasMany(Team::class, "id", "team_id");
    }

}
